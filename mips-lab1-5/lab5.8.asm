.data

word1: .word 0x89abcdef

.text
.globl main
main:
	la $a0, word1
	lwr $t4, 0($a0)		# load value into $t4
	lwr $t5, 1($a0)		# load value into $t5
	lwr $t6, 2($a0)		# load value into $t6
	lwr $t7, 3($a0)		# load value into $t7

	jr $ra
	