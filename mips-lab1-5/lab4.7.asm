.data
msg1: .asciiz "please input a non-negative integer: "  	# msg1 is a string ask user input 
msg2: .asciiz "please input another non-negative integer: "  	# msg2 is a string ask user input
msg3: .asciiz "the answer is: " 				# msg3 the result message
msg4: .asciiz "error. input is negative, please change  \n"  	# msg4 is warnning


.text
.globl main

main:        # ask user enter integer
	li $v0, 4 		            	# system call for print_str
	la $a0, msg1	           	# load msg1 to $a0
	syscall			# ask user input integer 

	li $v0, 5                                  	# read_int syscall
	syscall
	add $t0, $t0, $v0                       	# save input 1 in $t0

	li $v0, 4 		            	# system call for print_str
	la $a0, msg1	           	# load msg1 to $a0
	syscall			# ask user input integer 

	li $v0, 5                                  	# read_int syscall
	syscall
	add $t1, $t1, $v0                       	# save input 2 in $t1

	# test input
	bltz $t0, negative
	bltz $t1, negative	
	
	j Pass	

negative:
	li $v0, 4 		            	# system call for print_str
	la $a0, msg4	           	# load msg2 to $a0
	syscall			# ask user input integer
	j main			# jump back to main

Pass:
	# print result message
	li $v0, 4 		            	# system call for print_str
	la $a0, msg3	           	# load msg3 to $a0
	syscall			# result message

	addi $sp, $sp, -4		# save return address
	sw $ra, 0($sp)		# save $ra
	
	# pass argument
	move $a0, $t0		# pass first argument
	move $a1, $t1		# pass second argument

	jal Ackermann		# call Achermann
	
	move $a0, $v0		# load result

	# print result number
	li $v0, 1 		            	# system call for print_str
	syscall			# result message

	lw $ra, 0($sp)		# restore $ra
	addi $sp, $sp, 4
	jr $ra			# returen


Ackermann:
	# case1, x = 0
	beqz $a0, case1		# branch case1 if $a0 = 0
	
	# save return address on stack
	addi $sp, $sp, -4		# new stack
	sw $ra, 0($sp)		# save $ra on stack

	# case2, y = 0
	beqz $a1, case2		# branch case2 if $a1 = 0
	
	# else
	addi $t0, $a0, -1		# (x-1)
	addi $sp, $sp, -4		
	sw $t0, 0($sp)		# save x - 1 in stack
	
	addi $a1, $a1, -1		# y - 1
	jal Ackermann		# A(x,y-1)

	lw $a0, 0($sp)		# load x - 1 back
	addi $sp, $sp, 4		# restore sp
	
	move $a1, $v0		# y = A(x,y-1)
	jal Ackermann
	j finish

case1:
	addi $v0, $a1, 1		# result = y + 1
	jr $ra			# jump back


case2:
	addi $a0, $a0, -1		# x - 1
	li $a1, 1			# y = 1
	jal Ackermann		# recurrence

finish:
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra












