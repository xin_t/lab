.data 0x10010000
var1: .word 0x10 # var1 is a word (32 bit) with the initial value 0x10

var2: .word 0x20 # var2 is a word (32 bit) with the initial value 0x20

var3: .word 0x30 # var3 is a word (32 bit) with the initial value 0x30

var4: .word 0x40 # var4 is a word (32 bit) with the initial value 0x40

first: .byte 'X' # first is a byte (8 bits) with the initial value 'X'

last: .byte 'T'  # last is a byte (8 bits) with the initial value 'T'

.text
.globl main
main: addu, $s0, $ra, $0 # save $31 in $16
		lui $t0, 4097		#load var1 to $t0
		lw	$t4, 0($t0)
		
		lui $t0, 4097		#load var1 to $t1
		lw	$t5, 4($t0)
		
		lui $t0, 4097		#load var1 to $t2
		lw	$t6, 8($t0)
		
		lui $t0, 4097		#load var1 to $t3
		lw	$t7, 12($t0)
		
		lui $t0, 4097		#load t0 to var4
		sw	$t7, 0($t0)
		
		lui $t0, 4097		#load t1 to var3
		sw	$t6, 4($t0)
		
		lui $t0, 4097		#load t2 to var2
		sw	$t5, 8($t0)
		
		lui $t0, 4097		#load t3 to var1
		sw	$t4, 12($t0)
                    # restore the return address in $ra and return from main
addu $ra, $0, $s0   # return address back in $31
jr $ra              # return from main