# this is a program used to test memory alignment for data

.data 0x10000000
char1: .byte 'a' 		# reserve space for a byte
double1: .double 1.1 	# reserve space for a double
char2: .byte 'b' 		# b is 0x62 in ASCII
half1: .half 0x8001		# reserve space for a half-word (2 bytes)
char3: .byte 'c' 		# c is 0x63 in ASCII
word1: .word 0x56789abc 	# reserve space for a word
char4: .byte 'd'		# d is 0x64 in ASCII
word2: .word 0 		# word 2 is a a word with intial 0

.text
.globl main
main: 
la $s0, word1		# load address in $s0
lb $t0, 0($s0)		# load the first byte of word1
lb $t1, 1($s0)		# load the second byte of word1
lb $t2, 2($s0)		# load the third byte of word1
lb $t3, 3($s0)		# load the forth byte of word1

lbu $t4, 0($s0)		# load the first byte of word1
lbu $t5, 1($s0)		# load the second byte of word1
lbu $t6, 2($s0)		# load the third byte of word1
lbu $t7, 3($s0)		# load the forth byte of word1

lh $t8, half1		# load half1 in $t8
lhu $t9, half1		# load half1 in $t8

la $s1, word2		# load address in $s1
sb $t0, 3($s1)		# store $t0 into memory
sb $t1, 2($s1)		# store $t1 into memory
sb $t2, 1($s1)		# store $t2 into memory
sb $t3, 0($s1)		# store $t3 into memory

lw $s2, word2		# check word2 value

jr $ra 		# return from main