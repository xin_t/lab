.data
msg1: .asciiz "please input a non-negative integer"  	 	# msg1 is a string ask user input 
msg2: .asciiz "error. input is negative, please change"  		# msg2 is a string ask user input
msg3: .asciiz "the factorial of the number you entered is :" 		# msg3 the result message

.text
.globl main

main:        # ask user enter integer
	li $v0, 4 		            	# system call for print_str
	la $a0, msg1	           	# load msg1 to $a0
	syscall			# ask user input integer 

	li $v0, 5                                  	# read_int syscall
	syscall
	add $t0, $t0, $v0                       	# save input in $t0
	
	# test input
	slt $t1, $t0, $0		# set $t1 = 1 if $t0 is negative
	li $t2, 1			# branch condition
	beq $t1, $t2, negative	# branch if  $t1 = 1

	add $a0, $t0, $0		# pass the argument

	addi $sp, $sp, -4		# subtract 4 from stack pointer get new sp
	sw $ra,  4($sp)		# save $ra into new stack pointer
	jal Factorial 		# call ��Factorial�� 
	
	# print out the result message
	addi $sp, $sp, -4
	sw $v0, 4($sp)		# backup $a0 on stack
	
	li $v0, 4 		            	# system call for print_str
	la $a0, msg3	           	# load msg3 to $a0
	syscall			# result message 
	
	lw $v0, 4($sp)		# restore $a0
	addi $sp, $sp, 4		# restore stack	
	add $s5, $v0, $0		# save result in $s5

	# print out result number
	li $v0, 1 		            	# system call for print_int
	add $a0, $s5, $0	           	# load integer 2 to $a0
	syscall			# print out integer 2 

	nop 			# execute this after ��test�� returns
	lw $ra, 4($sp) 		# restore $ra from sp
	addi $sp, $sp, 4		# restore $sp
	jr $ra 			# return from main

Factorial:
	subu $sp, $sp, 4
	sw $ra, 4($sp) 		# save the return address on stack
	beqz $a0, terminate 	# test for termination
	subu $sp, $sp, 4 		# do not terminate yet
	sw $a0, 4($sp) 		# save the parameter
	sub $a0, $a0, 1 		# will call with a smaller argument
	jal Factorial
	# after the termination condition is reached these lines
	# will be executed
	lw $t0, 4($sp) 		# the argument I have saved on stack
	mul $v0, $v0, $t0		# do the multiplication
	lw $ra, 8($sp) 		# prepare to return
	addu $sp, $sp, 8 		# I��ve popped 2 words (an address and
	jr $ra 			# .. an argument)

terminate:
	li $v0, 1 			# 0! = 1 is the return value
	lw $ra, 4($sp) 		# get the return address
	addu $sp, $sp, 4 		# adjust the stack pointer
	jr $ra 			# return

negative:
	li $v0, 4 		            	# system call for print_str
	la $a0, msg2	           	# load msg2 to $a0
	syscall			# ask user input integer
	j main			# jump back to main
