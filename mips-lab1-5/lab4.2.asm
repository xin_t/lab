.text
.globl main

main: 
	addi $sp, $sp, -4		# subtract 4 from stack pointer get new sp
	sw $ra,  0($sp)		# save $ra into new stack pointer
	jal test 			# call ��test�� with no parameters
	nop 			# execute this after ��test�� returns
	lw $ra, 0($sp) 		# restore $ra from sp
	addi $sp, $sp, 4		# restore $sp
	jr $ra 			# return from main

test: 
	nop 			# this is the procedure named ��test��
	jr $ra 			# return from this procedure