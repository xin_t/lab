.data
msg1: .asciiz "I'm far away"  # msg1 is a string with value "I'm far away"
msg2: .asciiz "I'm nearby"  # msg2 is a string with value "I'm nearby"
msg3: .asciiz "Please enter an integer: "  # msg3 is a string with value "Please enter an integer:"
msg4: .asciiz "Please enter another integer:"  # msg4 is a string with value "Please enter another integer:"

.text
.globl main
main: 

li $v0, 4 		            # system call for print_str
la $a0, msg3	            # load msg3 to $a0
syscall

li $v0, 5                                  # read_int syscall
syscall
move $t0, $v0                       # move value of $v0 into $t0


li $v0, 4 		            # system call for print_str
la $a0, msg4	            # load msg4 to $a0
syscall

li $v0, 5                                  # read_int syscall
syscall
move $t1, $v0                       # move value of $v0 into $t1

                                             #compare with two input integers 
beq $t0, $t1, far	           #branch if ($t0==$t1)

li $v0, 4                                 # print string
la $a0, msg2                         # print msg2: "I'm nearby"
syscall
j exit


far:
li $t6, 0x10010000               # put a far address in register
li $v0, 4
la $a0, msg1                        # "I'm far away"
syscall
jr $t6  # jump to far address
j exit


exit:
li $v0, 10
syscall