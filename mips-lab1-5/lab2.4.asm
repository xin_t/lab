.data 0x10010000
var1: .word 43 # var1 is a word (32 bit) with the initial value 43

var2: .word 21 # var2 is a word (32 bit) with the initial value 21

.extern ext1 4  # reserve space for ext1
.extern ext2 4  # reserve space for ext2

.text
.globl main
main: 
addu, $s0, $ra, $0 # save $31 in $16
lw $t0, var1   # load var1 to register t0
lw $t1, var2   # load var2 to register t1
sw $t0, ext1   # store value of var1 to address of ext1
sw $t1, ext2   # store value of var2 to address of ext2


                       # restore the return address in $ra and return from main
addu $ra, $0, $s0   # return address back in $31
jr $ra              # return from main