.data 0x10010000
my_array: .space 40  # reserve 40 bytes(10 words)
initial_value: .word 1


.text
.globl main

main: 
lw $t1, initial_value  # load initial to $t1
la $t0, my_array  # 
addi $a1, $t0, 40  # $a1 <- end loop condition

loop:
ble $a1, $t0 exit  # exit condition
sw $t1, 0($t0)  # my_array[i] = j 
addi $t1, $t1, 1  # j++1
addi $t0, $t0, 4  # next execute address
j loop



exit:
jr $ra