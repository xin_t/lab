.data
msg1: .asciiz "please input an integer"  	 	# msg1 is a string ask user input 
msg2: .asciiz "please input another integer"  	# msg2 is a string ask user input
msg3: .asciiz "the largest number is :" 
msg4: .asciiz "two integers are equal :" 



.text
.globl main

main: 
	li $v0, 4 		            	# system call for print_str
	la $a0, msg1	           	# load msg1 to $a0
	syscall			# ask user input integer 1

	li $v0, 5                                  	# read_int syscall
	syscall
	add $t0, $t0, $v0                       	# save input in $t0

	li $v0, 4 		            	# system call for print_str
	la $a0, msg2	           	# load msg2 to $a0
	syscall			# ask user input integer 2

	li $v0, 5                                  	# read_int syscall
	syscall
	add $t1, $t1, $v0                       	# save input in $t1

	addi $sp, $sp, -4		# reserve space in stack pointer	
	sw $ra, 0($sp)		# put $ra in stack pointer
	add $a0, $t0, $0		# pass integer 1 to $a0
	add $a1, $t1, $0		# pass integer 2 to $a1

	jal Largest 		# call Largest and update $ra
	
	lw $ra, 0($sp)		# get $ra back from sp
	addi $sp, $sp, 4		# restore sp
	j $ra			 			

Largest:
	
	sub $t3, $a0, $a1		# integer 1 - interger 2 -> $t3
	beq $t3, $0, equal                     # branch to equal if $t3 = 0
	slt $t4, $0, $t3		# set $t4 = 1 if 0 < $t3, set $t4 = 0 if 0 > $t3
	beq $t4, 0, smaller		# branch to smaller means integer 2 is the largest

	addi $sp, $sp, -4		# new stack pointer
	sw $a0, 0($sp)		# backup $a0 in stack pointer
	li $v0, 4 		            	# system call for print_str
	la $a0, msg3	           	# load msg1 to $a0
	syscall			# the largest number is 
	
	lw $a0, 0($sp)		# restore $a0
	addi $sp, $sp, 4		# restore $sp
	li $v0, 1 		            	# system call for print_int
	syscall			# print out integer 1 

	jr $ra			# jump back to main

equal:
	addi $sp, $sp, -4		# new stack pointer
	sw $a0, 0($sp)		# backup $a0 in stack pointer
	li $v0, 4 		            	# system call for print_str
	la $a0, msg4	           	# load msg4 to $a0
	syscall			# equal

	lw $a0, 0($sp)		# restore $a0
	addi $sp, $sp, 4		# restore $sp
	li $v0, 1 		            	# system call for print_int
	syscall			# print out integer

	jr $ra			# jump back to main

smaller:
	addi $sp, $sp, -4		# new stack pointer
	sw $a0, 0($sp)		# backup $a0 in stack pointer
	li $v0, 4 		            	# system call for print_str
	la $a0, msg3	           	# load msg3 to $a0
	syscall			# largest is:
	
	lw $a0, 0($sp)		# restore $a0
	addi $sp, $sp, 4		# restore $sp
	li $v0, 1 		            	# system call for print_int
	add $a0, $a1, $0	           	# load integer 2 to $a0
	syscall			# print out integer 2 
	jr $ra			# jump back to main



