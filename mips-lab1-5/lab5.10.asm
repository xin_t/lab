# this is a program used to test memory alignment for data

.data 0x10000000
.align 0
ch1: .byte 'a' 		# reserve space for a byte
word1: .word 0x89abcdef 	# reserve space for a word
ch2: .byte 'b'		# reserve space for a byte
word2: .word 0		# reserve space for a word

.text
.globl main
main: 

lui $a0, 0x1000	# load address of word1 to $a0
ori $a0, 0x1
lui $a1, 0x1000	# load address of word2 to $a1
ori $a1, 0x6
lwl $t0, 3($a0)	# load world to $t0
lwr $t0, 0($a0)	# load world to $t0
swl $t0, 3($a1)	# store value to world2
swr $t0, 0($a1)	# store value to world2

ori $v0, $0, 10	# exit
syscall