.data 0x10010000
var1: .word 2  # reserve space for variable var1 with intial value 2
var2: .word 4  # reserve space for variable var2 with intial value 4
var3: .word -2020  # reserve space for variable var3 with intial value -2020

.text
.globl main
main: 
#if section
lw $t0, var1  # load var1 to register t0
lw $t1, var2  # load var2 to register t1
bne $t0, $t1, else  # if $t0 != $t1, got to 'else' label

# then section
lw $t2, var3  # load var1 to register t2
sw $t2, var1  # store value in register t2 to var1
sw $t2, var2  # store value in register t2 to var2
beq $0, $0, final

else:
move $t2, $t0  # wasp the values of var1 and var2
move $t0, $t1
move $t1, $t2
sw $t0, var1
sw $t1, var2

final:
li $v0, 10
syscall