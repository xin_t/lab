.data
msg1: .asciiz "please input an integer: "  	 	# msg1 is a string ask user input 
user1: .word 0 				# reserve space of word
msg2: .asciiz "If bytes were layed in reverse order the number would be: " # result message

.text
.globl main

main: 
	addi $sp, $sp, -4		# subtract 4 from stack pointer get new sp
	sw $ra,  0($sp)		# save $ra into new stack pointer

	li $v0, 4 		            	# system call for print_str
	la $a0, msg1	           	# load msg1 to $a0
	syscall			# ask user input integer 1

	li $v0, 5                                  	# read_int syscall
	syscall
	sw $v0, user1                      	# save input in user1

	la $a0, user1		# pass argument
	jal Reverse_bytes 		# call procedure
	
	li $v0, 4 		            	# system call for print_str
	la $a0, msg2	           	# load msg1 to $a0
	syscall			# result message

	lw $t4, user1
	li $v0, 1 		            	# system call for print_int
	add $a0, $t4, $0
	syscall			# result message

	lw $ra, 0($sp) 		# restore $ra from sp
	addi $sp, $sp, 4		# restore $sp
	jr $ra 			# return from main

Reverse_bytes: 
	lb $t0, 0($a0)		# load byte 
	lb $t1, 1($a0)		# load byte 
	lb $t2, 2($a0)		# load byte 
	lb $t3, 3($a0)		# load byte 
	sb $t3, 0($a0)		# store byte to $a0
	sb $t2, 1($a0)		# store byte to $a0
	sb $t1, 2($a0)		# store byte to $a0
	sb $t0, 3($a0)		# store byte to $a0


	jr $ra 			# return from this procedure