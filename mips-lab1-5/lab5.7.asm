.data

word1: .word 0x89abcdef

.text
.globl main
main:
	la $a0, word1
	lwl $t0, 0($a0)		# load value into $t0
	lwl $t1, 1($a0)		# load value into $t1
	lwl $t2, 2($a0)		# load value into $t2
	lwl $t3, 3($a0)		# load value into $t3

	jr $ra
	