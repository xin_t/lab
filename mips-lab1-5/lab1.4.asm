.data 0x10000000
msg1: .asciiz "Please enter a double number: "
.text
.globl main
# Inside main there are some calls (syscall) which will change the
# value in $31 ($ra) which initially contains the return address
# from main. This needs to be saved.
main:  # spim starts here
li $v0, 4 # system call for print_str
la $a0, msg1 # address of string to print
syscall
# now get a float from the user
li $v0, 7 # system call for read_double
syscall # the float placed in $f0
# move the float out of $f0
mov.d $f8, $f0 # move the number into $f8
# print the result
li $v0, 3 # system call for print_float
mov.d $f12, $f8 # move number to print in $f12
syscall
# exit
li $v0, 10
syscall
