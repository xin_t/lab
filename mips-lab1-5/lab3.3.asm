.data 0x10010000
var1: .word 1  # reserve space for variable var1 with intial value 1

.text
.globl main
main: 
lw $t1, var1  # load var1
move $t0, $t1  # set i
li $a1, 100

loop:
ble $a1, $t0 exit  # if $t0 >= 100, exit loop
addi $t1, $t1, 1  # var1 = var1 + 1
addi $t0, $t0, 1  # i ++
j loop

exit:
sw $t1, var1
li $v0, 1
lw $a0, var1
syscall
jr $ra