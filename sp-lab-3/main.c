#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>



// load file function
FILE *read_file(char *fname){
    if(fopen(fname,"r") == NULL){
        printf("Fail to open");
        exit(EXIT_FAILURE);
    }
    return fopen(fname,"r");
}

// mean function
double mean_function(float *array, int size){
    double sum = 0;
    for (int i = 0; i < size; ++i) {
        sum = sum + array[i];
    }
    double mean = sum / size;
    return mean;
}

// SD function
double sd_function(float *array, int size){
    // sqrt((sum((xi - mean)^2))/N)
    double sd = 0;
    double mean =  mean_function(array,size);
    for (int i = 0; i < size; ++i) {
        sd += pow(array[i] - mean, 2);
    }
    return sqrt(sd/size);
}

// quick sort function
void quick_sort(float *array, int left, int right){
    float pivot = array[(left+right)/2];
    int i = left;
    int j = right;
    int temp;

    while (i<=j){
        while (array[i] < pivot){
            i ++;
        }
        while (array[j] > pivot){
            j --;
        }
        if (i <= j){
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i ++;
            j --;
        }
    }

    if (left < j){
        quick_sort(array,left,j);
    }
    if (i < right){
        quick_sort(array, i, right);
    }
}



// median function
double median_function(float *array, int size){
    quick_sort(array, 0, size - 1);
    if (size%2 == 1){
        return array[size/2];
    } else{
        double small = array[size/2 - 1];
        double large = array[size/2];
        return (small + large) / 2;
    }
}





int main(int argc, char *argv[]) {
    if (argc < 2){
        printf("enter file name\n");
        return 0;
    }

    int size = 20, length_array = 0;
    float *array = (float *) malloc(size * sizeof(float));

    char *fname = argv[1];
    FILE *fp = read_file(fname);

    int capacity_count = 0;

    while (!feof(fp)){
        // load data into array
        fscanf(fp, "%f\n",(array + length_array));
        length_array ++;
        // test space left
        if (length_array == size){  // array size is equal to setting
            float *new_array = (float *) malloc(size * 2 * sizeof(float ));
            // move data from old to new array
            memcpy(new_array, array, length_array * sizeof(float));
            // empty old array
            free(array);
            array = new_array;
            // increase size
            size = size * 2;
            capacity_count += 1;
        }
    }
    // calculate space left
    int capacity = 20 * pow(2,capacity_count) - length_array;

    fclose(fp);

    // print out result
    printf("Result: \n");
    printf("----------------\n");
    printf("Num values   : %d\n",length_array);
    double mean = mean_function(array, length_array);
    double sd = sd_function(array,length_array);
    printf("The mean is  : %.3f\n", mean);
    printf("The SD is    : %.3f\n", sd);
    double median = median_function(array, length_array);
    printf("The median is: %.3f\n", median);
    printf("----------------\n");
    printf("Unused array capacity: %d", capacity);
    return 0;
}
