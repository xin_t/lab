cmake_minimum_required(VERSION 3.17)
project(sp_lab_3 C)

set(CMAKE_C_STANDARD 99)

add_executable(sp_lab_3 main.c)