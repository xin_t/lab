

//
// Created by Administrator on 2020/11/10.
//

#ifndef SPLAB1_FUNCTION_H
#define SPLAB1_FUNCTION_H

struct Employee{
    int ID[20]; //  from 100000 to 999999
    char First_Name[20];
    char Last_Name[20];
    int Salary[20]; // from $30,000 to $150,000
};


// welcome page
void welcome();

// input file name return file address, provide suggest file
int *open_file();

// input file pointer print the file out
void print_data(int *fp);

// ask user input id, search
void IDlookup(int *fp);

// ask user input last name, search
void LastNameLookup(int *fp);

// ask user information if they want to add an employee
void AddEmployee(int *fp);

// compare function for sorting structure
int compareID(const void *a, const void *b);

// compare function for sorting structure based on salary
int compareSalary(const void *a, const void *b);

// remove an employee by id
void RemoveByID(int *fp);

// update information by ID
void InfoUpdate(int *fp);

// print out top # salary
void SalaryPrint(int *fp);

// input last name find all match
void LastNameAll(int *fp);


#endif //SPLAB1_FUNCTION_H
