

//
// Created by Administrator on 2020/11/10.
//

#include "function.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void welcome(){
    printf("Employee DB Menu \n");
    printf("-----------------------\n");
    printf("(1) Print the Database\n");
    printf("(2) Lookup by ID\n");
    printf("(3) Lookup by Last Name\n");
    printf("(4) Add an employee\n");
    printf("(5) Quit\n");
    printf("(6) Remove\n");
    printf("(7) Update Information\n");
    printf("(8) Print top # salary\n");
    printf("(9) Show all same last name employee\n");
    printf("-----------------------\n");
}

int *open_file(){
    FILE *fp;
    int select = 0;
    printf("Open file menu \n");
    printf("-----------------------\n");
    printf("(1) Open suggest file <data.txt> \n");
    printf("(2) type in new file name\n");
    printf("-----------------------\n");
    while (select != 9) {
        printf("Please choose: ");
        scanf("%d",&select);
        switch (select) {
            case 1:
                fp = fopen("data.txt", "r");
                if (fp == NULL) {
                    printf("Fail to open");
                    exit(EXIT_FAILURE);
                }
                select = 9;
                break;
            case 2:
                printf("Plese entre new file name: ");
                char name[20];
                scanf("%s", &name);
                fp = fopen(name, "r");
                if (fp == NULL) {
                    printf("Fail to open");
                    exit(EXIT_FAILURE);
                }
                select = 9;
                break;
        }
    }
    return fp;
}

void print_data(int *fp){
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    qsort(employee,i,sizeof(struct Employee),compareID);
    printf("\n");
    printf("Name                  Salary      ID\n");
    printf("-----------------------------------------\n");
    int count = 0;
    for(int j=0; j<i;j++){
        printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
        count ++;
    }
    printf("-----------------------------------------\n");
    printf("There are %d people in the data\n",count);
    printf("-----------------------------------------\n");
    fclose(fp);
}

void IDlookup(int *fp){
    struct Employee employee[1024];
    int i = 0;
    // load information into structure
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    int id;
    // get id input from user
    while (1){
        printf("please input ID you want to look for: \n");
        scanf("%d",&id);
        if (id <= 999999 && id >= 100000){
            break;
        }
        printf("id is something between 100000 and 999999\n");
    }
    // turn integer id into character
    char ids[20];
    sprintf(ids,"%d",id);

    int count = 0;  // set
    for(int j=0; j<i; j++){
        // compare input id with data id
        if (strcmp(ids,employee[j].ID) == 0 ){
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
            printf("-----------------------------------------\n");
            count ++;
            break;
        }
    }
    // display message if not found
    if (count == 0){
        printf("The ID can not be found\n");
    }

    fclose(fp);
}

void LastNameLookup(int *fp){
    struct Employee employee[1024];
    int i = 0;
    // load file data into structure
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    // ask user input last name
    char name[20];
    printf("please input last name you want to look for: \n");
    scanf("%s",&name);

    int count = 0;

    for(int j=0; j<i; j++){
        if (strcmp(name,employee[j].Last_Name) == 0 ){
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
            printf("-----------------------------------------\n");
            count ++;
            break;
        }
    }
    if (count == 0){
        printf("The last name can not be found\n");
    }

    fclose(fp);
}

void AddEmployee(int *fp){
    printf("Add an employee\n");
    // find current largest ID
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    qsort(employee,i,sizeof(struct Employee),compareID);
    fclose(fp);
    int largeID = atoi(employee[i-1].ID);
    // open file as append
    FILE *fw;
    fw = fopen("data.txt","a");
    if (fw == NULL){
        printf("Fail to open");
        exit(EXIT_FAILURE);
    }
    // define input variable
    int id;
    char first_name[10];
    char last_name[10];
    int salary;
    printf("Please input new employee information: \n");
    // input new id
    while (1){
        printf("please entre new id: ");
        scanf("%d", &id);
        if (id > largeID){
            if (id >= 100000 && id <= 999999){
                break;
            }
        }
        printf("ID is a number between 100000 and 999999\n");
        printf("New ID must greater than %d\n",largeID);
    }
    //input new first name
    printf("please entre new first name: ");
    scanf("%s", &first_name);
    //input new last name
    printf("please entre new last name: ");
    scanf("%s", &last_name);
    // input new salary
    while (1){
        printf("please entre new salary: ");
        scanf("%d", &salary);
        if (salary >= 30000 && salary <= 150000){
            break;
        }
        printf("salary is between 30000 and 150000");
    }
    // update the file
    fprintf(fw,"%d %s %s %d \n",id,first_name,last_name,salary);
    fclose(fw);
}

int compareID(const void *a, const void *b){
    const struct Employee *p1 = a;
    const struct Employee *p2 = b;
    int pp1 = atoi(p1->ID);
    int pp2 = atoi(p2->ID);
    return pp1-pp2;
}

int compareSalary(const void *a, const void *b){
    const struct Employee *p1 = a;
    const struct Employee *p2 = b;
    int pp1 = atoi(p1->Salary);
    int pp2 = atoi(p2->Salary);
    return pp2-pp1;
}

void RemoveByID(int *fp){
    // first load data into structure
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    int element = i;
    fclose(fp);

    // find the id looking for
    int id;
    // get id input from user
    while (1){
        printf("please input ID you want to look for: \n");
        scanf("%d",&id);
        if (id <= 999999 && id >= 100000){
            break;
        }
        printf("id is something between 100000 and 999999\n");
    }
    // turn integer id into character
    char ids[20];
    sprintf(ids,"%d",id);

    int exit = 0;  // set
    for(int j=0; j<element; j++){
        // compare input id with data id
        if (strcmp(ids,employee[j].ID) == 0 ){
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
            printf("-----------------------------------------\n");
            exit = j;
            int count = 0;
            int d;
            int getout = 1;
            while (getout){
                printf("Are you sure to delete this employee ? \n");
                printf("(1) Yes\n");
                printf("(2) No\n");
                scanf("%d",&d);
                if (d == 2){
                    getout = 0;
                }
                if (d == 1){
                    for (int k = j; k < i-1; ++k) {
                        employee[k] = employee[k+1];
                    }
                    printf("\n");
                    printf("Name                  Salary      ID\n");
                    printf("-----------------------------------------\n");
                    remove("data.txt");
                    // make new file
                    FILE *fa;
                    fa = fopen("data.txt","a");
                    for (int m = 0; m < i-1; ++m) {
                        printf("%-10s %-10s %-10s %-10s\n",&employee[m].First_Name,&employee[m].Last_Name,&employee[m].Salary,&employee[m].ID);
                        fprintf(fa,"%s %s %s %s \n",&employee[m].ID,&employee[m].First_Name,&employee[m].Last_Name,&employee[m].Salary);
                        count ++;
                    }
                    printf("-----------------------------------------\n");
                    printf("There are %d people in the data\n",count);
                    printf("-----------------------------------------\n");
                    // write the new data into file
                    printf("File updated\n");
                    fclose(fa);
                    getout = 0;
                    break;
                }
            }
            break;
        }
    }
    if(exit == 0){
        printf("ID is not exit\n");
    }
}

void InfoUpdate(int *fp){
    // first load data into structure
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    int element = i;

    // fclose(fp);
    qsort(employee,i,sizeof(struct Employee),compareID);
    fclose(fp);
    int largeID = atoi(employee[i-1].ID);

    // find the id looking for
    int id;
    // get id input from user
    while (1){
        printf("please input ID you want to look for: \n");
        scanf("%d",&id);
        if (id <= 999999 && id >= 100000){
            break;
        }
        printf("id is something between 100000 and 999999\n");
    }
    // turn integer id into character
    char ids[20];
    sprintf(ids,"%d",id);

    int exit = 0;  // set
    for(int j=0; j<i; j++){
        // compare input id with data id
        if (strcmp(ids,employee[j].ID) == 0 ){
            // print out the target employee
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
            printf("-----------------------------------------\n");
            exit = j;
            int count = 0;
            int d = 0;
            // delete old file
            remove("data.txt");
            // make new file
            FILE *fa;
            fa = fopen("data.txt","a");
            // provide option
            while (d != 4){
                printf("Select which information you want to change \n");
                printf("(1) Name\n");
                printf("(2) ID\n");
                printf("(3) salary\n");
                printf("(4) exit\n");
                scanf("%d",&d);

                switch (d) {
                    case 1:
                        //input new first name
                        printf("please entre new first name: ");
                        scanf("%s", &employee[j].First_Name);
                        //input new last name
                        printf("please entre new last name: ");
                        scanf("%s", &employee[j].Last_Name);
                        // print update info
                        printf("\n");
                        printf("Name                  Salary      ID\n");
                        printf("-----------------------------------------\n");
                        printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
                        printf("-----------------------------------------\n");
                        break;
                    case 2:
                        printf("Please input new employee information: \n");
                        // input new id
                        while (1){
                            printf("please entre new id: ");
                            scanf("%s", &employee[j].ID);
                            if (*employee[j].ID > largeID){
                                if (id >= 100000 && id <= 999999){
                                    break;
                                }
                            }
                            printf("ID is a number between 100000 and 999999\n");
                            printf("New ID must greater than %d\n",largeID);
                        }
                        // print update info
                        printf("\n");
                        printf("Name                  Salary      ID\n");
                        printf("-----------------------------------------\n");
                        printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
                        printf("-----------------------------------------\n");
                        break;
                    case 3:
                        // input new salary
                        while (1){
                            printf("please entre new salary: ");
                            scanf("%s", &employee[j].Salary);
                            if (*employee[j].Salary >= 30000 && *employee[j].Salary <= 150000){
                                break;
                            }
                            printf("salary is between 30000 and 150000");
                        }
                        // print update info
                        printf("\n");
                        printf("Name                  Salary      ID\n");
                        printf("-----------------------------------------\n");
                        printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
                        printf("-----------------------------------------\n");
                        break;
                }

            }
            for (int m = 0; m < i; ++m) {
                printf("%-10s %-10s %-10s %-10s\n",&employee[m].First_Name,&employee[m].Last_Name,&employee[m].Salary,&employee[m].ID);
                fprintf(fa,"%s %s %s %s \n",&employee[m].ID,&employee[m].First_Name,&employee[m].Last_Name,&employee[m].Salary);
                count ++;
            }
            printf("-----------------------------------------\n");
            printf("There are %d people in the data\n",count);
            printf("-----------------------------------------\n");
            // write the new data into file
            printf("File updated\n");
            fclose(fa);
            break;
        }
    }
    if(exit == 0){
        printf("ID is not exit\n");
    }

}

void SalaryPrint(int *fp){
    // first load data into structure
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    qsort(employee,i,sizeof(struct Employee),compareSalary);
    int top;
    int exit = 1;
    while (exit){
        printf("Top # salary print out\n");
        printf("Please type in to choose how many information you want: (a number between 1 and %d)\n",i-1);
        scanf("%d",&top);
        if (top > 0 && top < i){
            qsort(employee,top,sizeof(struct Employee),compareID);
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            int count = 0;
            for(int j=0; j<top;j++){
                printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
                count ++;
            }
            printf("-----------------------------------------\n");
            printf("There are %d people in the data\n",count);
            printf("-----------------------------------------\n");
            exit = 0;
        }
    }
    fclose(fp);
}

void LastNameAll(int *fp){
    // load data into structure
    struct Employee employee[1024];
    int i = 0;
    while (!feof(fp)){
        fscanf(fp,"%s %s %s %s\n", &employee[i].ID,&employee[i].First_Name,&employee[i].Last_Name,&employee[i].Salary);
        i++;
    }
    qsort(employee,i,sizeof(struct Employee),compareID);
    // ask user input last name
    char name[20];
    printf("please input last name you want to look for: \n");
    scanf("%s",&name);

    int count = 0;

    for(int j=0; j<i; j++){
        if (strcmp(name,employee[j].Last_Name) == 0 ){
            printf("\n");
            printf("Name                  Salary      ID\n");
            printf("-----------------------------------------\n");
            printf("%-10s %-10s %-10s %-10s\n",&employee[j].First_Name,&employee[j].Last_Name,&employee[j].Salary,&employee[j].ID);
            printf("-----------------------------------------\n");
            count = j;
        }
    }
    if (count == 0){
        printf("The last name can not be found\n");
    }
    fclose(fp);
}
