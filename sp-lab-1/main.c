#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "function.h"



int main(void)
{
    // Welcome page
    open_file();
    // Load data file
    int select = 0;
    while (select != 5){
        FILE *fp;
        fp = fopen("data.txt", "r");
        // display menu
        welcome();
        // let user select
        printf("Enter your choice: ");
        scanf("%d",&select);
        switch (select) {
            case 1:  // print out data
                print_data(fp);
                break;
            case 2:
                IDlookup(fp);
                break;
            case 3:
                LastNameLookup(fp);
                break;
            case 4:
                AddEmployee(fp);
                break;
        }

    }
    printf("Goodbye!");
    return 0;
}
